package repository

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
)

type ReservaRepository interface {
	ConsultarReserva(ID string) (models.Reserva, error)
	EditarReserva(reserv models.Reserva, ID string) (bool, error)
	InsertoRegistroReserva(v models.Reserva) (string, bool, error)
	GetAllReserva() ([]models.ReservaMod, error)
	CambiarEstadoReserva(user models.Reserva, ID string) (bool, error)
}
