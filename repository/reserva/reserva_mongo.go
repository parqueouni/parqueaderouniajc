package Reserva

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository"
	"context"
	"fmt"
	"time"

	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type mongReservaRepo struct {
	Conn *bd.Data
}

// NewReservaRepo returns implement of reserva repository interface
func NewReservaRepo(Conn *bd.Data) repository.ReservaRepository {
	return &mongReservaRepo{
		Conn: Conn,
	}
}

/*InsertoRegistroReserva es la para final con la BD para insertar los datos del Reserva */
func (ur *mongReservaRepo) InsertoRegistroReserva(u models.Reserva) (string, bool, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("reserva")

	result, err := col.InsertOne(ctx, u)
	if err != nil {
		return "", false, err
	}

	ObjID, _ := result.InsertedID.(primitive.ObjectID)
	return ObjID.String(), true, nil
}

/*EditarReserva modificar datos del Reserva*/
func (ur *mongReservaRepo) EditarReserva(reserv models.Reserva, ID string) (bool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("reserva")

	registro := make(map[string]interface{})

	if len(reserv.Fecha) > 0 {
		registro["fecha"] = reserv.Fecha
	}
	if len(reserv.Hora) > 0 {
		registro["hora"] = reserv.Hora
	}
	if len(reserv.Estado) > 0 {
		registro["estado"] = reserv.Estado
	}

	updtString := bson.M{
		"$set": registro,
	}
	objID, _ := primitive.ObjectIDFromHex(ID)
	filtro := bson.M{"_id": bson.M{"$eq": objID}}

	_, err := col.UpdateOne(ctx, filtro, updtString)

	if err != nil {
		return false, err
	}
	return true, nil
}

/*CambiarEstadoReserva modificar estado del Reserva*/
func (ur *mongReservaRepo) CambiarEstadoReserva(user models.Reserva, ID string) (bool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("reserva")

	registro := make(map[string]interface{})

	if len(user.Estado) > 0 {
		if user.Estado == "Activo" {
			registro["Estado"] = "Inactivo"
		} else {
			registro["Estado"] = "Activo"
		}

	}

	updtString := bson.M{
		"$set": registro,
	}
	objID, _ := primitive.ObjectIDFromHex(ID)
	filtro := bson.M{"_id": bson.M{"$eq": objID}}

	_, err := col.UpdateOne(ctx, filtro, updtString)

	if err != nil {
		return false, err
	}
	return true, nil
}

/*ConsultarReserva consultar Reserva por id*/
func (ur *mongReservaRepo) ConsultarReserva(ID string) (models.Reserva, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("Reserva")

	var Reserva models.Reserva
	objID, _ := primitive.ObjectIDFromHex(ID)
	condicion := bson.M{
		"_id": objID,
	}
	err := col.FindOne(ctx, condicion).Decode(&Reserva)
	if err != nil {
		fmt.Println("Registro no encontrado" + err.Error())
		return Reserva, err
	}
	return Reserva, nil
}

// GetAllReserva returns all users.
func (ur *mongReservaRepo) GetAllReserva() ([]models.ReservaMod, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("reserva")
	col1 := db.Collection("usuario")
	col2 := db.Collection("vehiculo")

	var ReservasReturn []models.ReservaMod

	condicion := bson.M{

	}
	row,err := col.Find(ctx, condicion)

	if err != nil {
		fmt.Println("Registros no encontrados" + err.Error())
		return ReservasReturn, err
	}

	for row.Next(context.TODO()) {
		var registro models.Reserva
		err := row.Decode(&registro)
		if err != nil {
			return ReservasReturn, err
		}
		var usuario models.Usuario
		objID, _ := primitive.ObjectIDFromHex(registro.IDUsuario)
		condicion := bson.M{
			"_id": objID,
		}
		err2 := col1.FindOne(ctx, condicion).Decode(&usuario)

		if err2 != nil {
			return ReservasReturn, err
		}
		var vehiculo models.Vehiculo
		objID2, _ := primitive.ObjectIDFromHex(registro.IDVehiculo)
		condicion2 := bson.M{
			"_id": objID2,
		}
		err3 := col2.FindOne(ctx, condicion2).Decode(&vehiculo)

		if err3 != nil {
			fmt.Println("Registro no encontrado" + err.Error())
			return ReservasReturn, err
		}

		var reg models.ReservaMod
		reg.ID=registro.ID
		reg.Hora=registro.Hora
		reg.Fecha=registro.Fecha
		reg.Estado=registro.Estado
		reg.Nombre=usuario.Nombre+" "+usuario.Apellido
		reg.Placa=vehiculo.Placa
		ReservasReturn = append(ReservasReturn, reg)
	}

	return ReservasReturn, nil
}