package repository

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type VehiculoRepository interface {
	ConsultarVehiculo(ID string) (models.Vehiculo, error)
	EditarVehiculo(vehiculo models.Vehiculo, ID primitive.ObjectID) (bool, error)
	InsertoRegistroVehiculo(v models.Vehiculo) (string, bool, error)
	GetAllvehiculo() ([]models.Vehiculo, error)
}
