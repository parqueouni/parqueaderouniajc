package usuario

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"

	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	"go.mongodb.org/mongo-driver/bson"
)

/*CheckYaExisteUsuario recibe un email de parámetro y chequea si ya está en la BD*/
func CheckYaExisteUsuario(email string) (models.Usuario, bool, string) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := bd.MongoCN.Database("parqueouniajc")
	col := db.Collection("usuario")

	condicion := bson.M{"email": email}

	var resultado models.Usuario

	err := col.FindOne(ctx, condicion).Decode(&resultado)
	ID := resultado.ID.Hex()
	if err != nil {
		return resultado, false, ID
	}
	return resultado, true, ID
}

/*CheckYaExisteVehiculo recibe una placa de parámetro y chequea si ya está en la BD*/
func CheckYaExisteVehiculo(placa string) (models.Vehiculo, bool, string) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := bd.MongoCN.Database("parqueouniajc")
	col := db.Collection("vehiculo")

	condicion := bson.M{"placa": placa}

	var resultado models.Vehiculo

	err := col.FindOne(ctx, condicion).Decode(&resultado)
	ID := resultado.ID.Hex()
	if err != nil {
		return resultado, false, ID
	}
	return resultado, true, ID
}

/*CheckYaExisteReserva recibe una id de parámetro y chequea si ya está en la BD*/
func CheckYaExisteReserva(ID primitive.ObjectID) (models.Reserva, bool, string) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := bd.MongoCN.Database("parqueouniajc")
	col := db.Collection("vehiculo")

	condicion := bson.M{"_id": ID}

	var resultado models.Reserva

	err := col.FindOne(ctx, condicion).Decode(&resultado)
	var IDs = resultado.ID.Hex()
	if err != nil {
		return resultado, false, IDs
	}
	return resultado, true, IDs
}
