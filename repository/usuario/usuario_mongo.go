package usuario

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository"
	"context"
	"fmt"
	"time"

	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

type mongUserRepo struct {
	Conn *bd.Data
}

// NewUserRepo returns implement of user repository interface
func NewUserRepo(Conn *bd.Data) repository.UserRepository {
	return &mongUserRepo{
		Conn: Conn,
	}
}

/*Login funcion para validar email y password para poder ingresar al sistema*/
func (ur *mongUserRepo) Login(email string, password string) (models.Usuario, bool) {

	usu, encontrado, _ := CheckYaExisteUsuario(email)
	if encontrado == false {
		return usu, false
	}

	passwordBytes := []byte(password)
	passwordBD := []byte(usu.Password)
	err := bcrypt.CompareHashAndPassword(passwordBD, passwordBytes)
	if err != nil {
		return usu, false
	}
	return usu, true
}

/*InsertoRegistro es la para final con la BD para insertar los datos del usuario */
func (ur *mongUserRepo) InsertoRegistro(u models.Usuario) (string, bool, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("usuario")

	u.Password, _ = bd.EncriptarPassword(u.Password)

	result, err := col.InsertOne(ctx, u)
	if err != nil {
		return "", false, err
	}

	ObjID, _ := result.InsertedID.(primitive.ObjectID)
	return ObjID.String(), true, nil
}

/*EditarUsuario modificar datos del usuario*/
func (ur *mongUserRepo) EditarUsuario(user models.Usuario, ID string) (bool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("usuario")

	registro := make(map[string]interface{})

	if len(user.Nombre) > 0 {
		registro["nombre"] = user.Nombre
	}
	if len(user.Apellido) > 0 {
		registro["apellido"] = user.Apellido
	}

	updtString := bson.M{
		"$set": registro,
	}
	objID, _ := primitive.ObjectIDFromHex(ID)
	filtro := bson.M{"_id": bson.M{"$eq": objID}}

	_, err := col.UpdateOne(ctx, filtro, updtString)

	if err != nil {
		return false, err
	}
	return true, nil
}

/*CambiarEstadousuario modificar estado del usuario*/
func (ur *mongUserRepo) CambiarEstadousuario(user models.Usuario, ID string) (bool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("usuario")

	registro := make(map[string]interface{})

	if len(user.Estado) > 0 {
		if user.Estado == "Activo" {
			registro["Estado"] = "Inactivo"
		} else {
			registro["Estado"] = "Activo"
		}

	}

	updtString := bson.M{
		"$set": registro,
	}
	objID, _ := primitive.ObjectIDFromHex(ID)
	filtro := bson.M{"_id": bson.M{"$eq": objID}}

	_, err := col.UpdateOne(ctx, filtro, updtString)

	if err != nil {
		return false, err
	}
	return true, nil
}

/*ConsultarUsuarioDB consultar usuario por id*/
func (ur *mongUserRepo) ConsultarUsuarioDB(ID string) (models.Usuario, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("usuario")

	var usuario models.Usuario
	objID, _ := primitive.ObjectIDFromHex(ID)
	condicion := bson.M{
		"_id": objID,
	}
	err := col.FindOne(ctx, condicion).Decode(&usuario)
	usuario.Password = ""
	if err != nil {
		fmt.Println("Registro no encontrado" + err.Error())
		return usuario, err
	}
	return usuario, nil
}

// GetAllUser returns all users.
func (ur *mongUserRepo) GetAllUser() ([]models.Usuario, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("usuario")

	var usuarios []models.Usuario

	condicion := bson.M{

	}
	row, err := col.Find(ctx, condicion)

	if err != nil {
		fmt.Println("Registros no encontrados" + err.Error())
		return usuarios, err
	}

	for row.Next(context.TODO()) {
		var registro models.Usuario
		err := row.Decode(&registro)
		if err != nil {
			return usuarios, err
		}
		usuarios = append(usuarios, registro)
	}

	return usuarios, nil
}
