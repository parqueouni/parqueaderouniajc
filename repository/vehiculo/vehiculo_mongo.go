package vehiculo

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/net/context"
	"time"

	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
)

type mongVehiculoRepo struct {
	Conn *bd.Data
}

// NewVehiculoRepo returns implement of user repository interface
func NewVehiculoRepo(Conn *bd.Data) repository.VehiculoRepository {
	return &mongVehiculoRepo{
		Conn: Conn,
	}
}

/*ConsultarVehiculo consultar vehiculo por id*/
func (vr *mongVehiculoRepo) ConsultarVehiculo(ID string) (models.Vehiculo, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	db := vr.Conn.DB.Database("parqueouniajc")
	col := db.Collection("vehiculo")

	var vehiculo models.Vehiculo
	objID, _ := primitive.ObjectIDFromHex(ID)
	condicion := bson.M{
		"_id": objID,
	}
	err := col.FindOne(ctx, condicion).Decode(&vehiculo)

	if err != nil {
		fmt.Println("Registro no encontrado" + err.Error())
		return vehiculo, err
	}
	return vehiculo, nil
}

/*EditarVehiculo modificar datos del vehiculo*/
func (vr *mongVehiculoRepo) EditarVehiculo(vehiculo models.Vehiculo, ID primitive.ObjectID) (bool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := vr.Conn.DB.Database("parqueouniajc")
	col := db.Collection("vehiculo")

	registro := make(map[string]interface{})

	if len(vehiculo.Placa) > 0 {
		registro["placa"] = vehiculo.Placa
	}
	if len(vehiculo.Modelo) > 0 {
		registro["modelo"] = vehiculo.Modelo
	}
	if len(vehiculo.Color) > 0 {
		registro["color"] = vehiculo.Color
	}
	if len(vehiculo.Tipo) > 0 {
		registro["tipo"] = vehiculo.Tipo
	}
	if len(vehiculo.Marca) > 0 {
		registro["marca"] = vehiculo.Marca
	}
	if len(vehiculo.Propietario) > 0 {
		registro["propietario"] = vehiculo.Propietario
	}

	updtString := bson.M{
		"$set": registro,
	}

	filtro := bson.M{"_id": bson.M{"$eq": ID}}

	_, err := col.UpdateOne(ctx, filtro, updtString)

	if err != nil {
		return false, err
	}
	return true, nil
}

/*InsertoRegistroVehiculo es la para final con la BD para insertar los datos del vehiculo */
func (vr *mongVehiculoRepo) InsertoRegistroVehiculo(v models.Vehiculo) (string, bool, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := vr.Conn.DB.Database("parqueouniajc")
	col := db.Collection("vehiculo")

	result, err := col.InsertOne(ctx, v)
	if err != nil {
		return "", false, err
	}

	ObjID, _ := result.InsertedID.(primitive.ObjectID)
	return ObjID.String(), true, nil
}

// GetAllvehiculo returns all users.
func (ur *mongVehiculoRepo) GetAllvehiculo() ([]models.Vehiculo, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	db := ur.Conn.DB.Database("parqueouniajc")
	col := db.Collection("vehiculo")

	var vehiculos []models.Vehiculo

	condicion := bson.M{

	}
	row, err := col.Find(ctx, condicion)

	if err != nil {
		fmt.Println("Registros no encontrados" + err.Error())
		return vehiculos, err
	}

	for row.Next(context.TODO()) {
		var registro models.Vehiculo
		err := row.Decode(&registro)
		if err != nil {
			return vehiculos, err
		}
		vehiculos = append(vehiculos, registro)
	}

	return vehiculos, nil
}
