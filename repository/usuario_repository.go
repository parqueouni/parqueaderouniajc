package repository

import "bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"

type UserRepository interface {
	Login(email string, password string) (models.Usuario, bool)
	InsertoRegistro(u models.Usuario) (string, bool, error)
	EditarUsuario(user models.Usuario, ID string) (bool, error)
	CambiarEstadousuario(user models.Usuario, ID string) (bool, error)
	ConsultarUsuarioDB(ID string) (models.Usuario, error)
	GetAllUser() ([]models.Usuario, error)
}
