package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

/*Reserva es el modelo de reserva de la base MongoDB */
type ReservaMod struct {
	ID         primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Nombre  string             `bson:"nombre" json:"nombre,omitempty"`
	Placa string             `bson:"placa" json:"placa,omitempty"`
	Fecha      string             `bson:"fecha" json:"fecha,omitempty"`
	Hora       string             `bson:"hora" json:"hora,omitempty"`
	Estado     string             `bson:"estado" json:"estado,omitempty"`
}
