package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

/*Reserva es el modelo de reserva de la base MongoDB */
type Reserva struct {
	ID         primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	IDUsuario  string             `bson:"idusuario" json:"idusuario,omitempty"`
	IDVehiculo string             `bson:"idvehiculo" json:"idvehiculo,omitempty"`
	Fecha      string             `bson:"fecha" json:"fecha,omitempty"`
	Hora       string             `bson:"hora" json:"hora,omitempty"`
	Estado     string             `bson:"estado" json:"estado,omitempty"`
}
