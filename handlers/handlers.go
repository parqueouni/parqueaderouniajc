package handlers

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	reservation "bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository/reserva"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository/usuario"
	veh "bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository/vehiculo"

	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/token"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/jwt"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository"
)

const Type = "application/json"
const CntxType = "content-type"
const Cors = "Access-Control-Allow-Origin"
const AllOrigin = "*"
const MsgError = "Error en los datos recibidos"
const MsgErrorConsult = "Ocurrio un error al intentar buscar el registro"
const MsgEntry = "Ingreso en insert"
const IncorrectData = "Datos Incorrectos "
const StatusError = "error estatus"
const UpdateRegistryOk = "Se edito el registro correctamente"
const MsgErrorEdit = "Ocurrión un error al intentar modificar el registro. Reintente nuevamente "
const ErrorErr = "error err";
const MsgErrorAllConsult = "Ocurrio un error al intentar buscar los registros"
// Route ...
type Route struct {
	Repo  repository.UserRepository
	Repo2 repository.VehiculoRepository
	Repo3 repository.ReservaRepository
}

// NewUserHandler ...
func NewUserHandler(db *bd.Data) *Route {
	return &Route{
		Repo:  usuario.NewUserRepo(db),
		Repo2: veh.NewVehiculoRepo(db),
		Repo3: reservation.NewReservaRepo(db),
	}
}

/*Login ruta de funcion para el login del usuario*/
func (urr *Route) Login(w http.ResponseWriter, r *http.Request) {
	var t models.Usuario
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, "Usuario y/o Contraseña inválidos "+err.Error(), 400)
		return
	}
	if len(t.Email) == 0 {
		http.Error(w, "El email del usuario es requerido ", 400)
		return
	}
	documento, existe := urr.Repo.Login(t.Email, t.Password)
	if existe == false {
		http.Error(w, "Usuario y/o Contraseña inválidos ", 400)
		return
	}

	jwtKey, err := jwt.GenerateJWT(documento)
	if err != nil {
		http.Error(w, "Ocurrió un error al intentar general el Token correspondiente "+err.Error(), 400)
		return
	}

	resp := models.RespuestaLogin{
		Token: jwtKey,
	}

	w.Header().Set(CntxType, Type)
	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(resp)

	expirationTime := time.Now().Add(24 * time.Hour)
	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   jwtKey,
		Expires: expirationTime,
	})
}

/*Registro es la función para crear en la BD el registro de usuario */
func (urr *Route) Registro(w http.ResponseWriter, r *http.Request) {
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	var t models.Usuario

	err := json.NewDecoder(r.Body).Decode(&t)
	print(t.Apellido)
	if err != nil {
		http.Error(w, MsgError+err.Error(), 400)
		return
	}
	if len(t.Email) == 0 {
		http.Error(w, "El Email de usuario es requerido", 400)
		return
	}
	if len(t.Password) < 6 {
		http.Error(w, "Debe colocar una contraseña minima de 6 caracteres", 400)
		return
	}

	_, encontrado, _ := usuario.CheckYaExisteUsuario(t.Email)
	if encontrado == true {
		http.Error(w, "Ya existe un usuario registrado con ese email", 400)
		return
	}

	_, status, err := urr.Repo.InsertoRegistro(t)
	if err != nil {
		http.Error(w, "Ocurrio un error al intentar realizar el registro de usuario"+err.Error(), 400)
		return
	} else {

		print(MsgEntry)
	}

	if status == false {
		http.Error(w, "No se ha logrado insertar el registro del usuario", 400)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Se creo el registro correctamente"))
}

/*CambiarEstadoUsuario editar datos del usuario*/
func (urr *Route) CambiarEstadoUsuario(w http.ResponseWriter, r *http.Request) {
	var t models.Usuario
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, IncorrectData+err.Error(), 400)
		print(IncorrectData)
		return
	}

	var status bool

	status, err = urr.Repo.EditarUsuario(t, token.IDUsuario)
	if err != nil {
		http.Error(w, MsgErrorEdit+err.Error(), 400)
		print(ErrorErr)
		return
	}

	if status == false {
		http.Error(w, "No se ha logrado cambiar el estado del usuario ", 400)
		print(StatusError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Se cambio el estado correctamente"))
}

/*ConsultarUsuario route para consultar usuario por ID*/
func (urr *Route) ConsultarUsuario(w http.ResponseWriter, r *http.Request) {
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	user, err := urr.Repo.ConsultarUsuarioDB(token.IDUsuario)
	if err != nil {
		http.Error(w, MsgErrorConsult+err.Error(), 400)
		return
	}

	w.Header().Set(CntxType, Type)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(user)
}

/*GetAllUser route para consultar todos los usuarios*/
func (urr *Route) GetAllUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	user, err := urr.Repo.GetAllUser()
	if err != nil {
		http.Error(w, MsgErrorAllConsult+err.Error(), 400)
		return
	}

	w.Header().Set(CntxType, Type)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(user)
}

/*EditarUsuario editar datos del usuario*/
func (urr *Route) EditarUsuario(w http.ResponseWriter, r *http.Request) {
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	var t models.Usuario

	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, IncorrectData+err.Error(), 400)
		print(IncorrectData)
		return
	}

	var status bool

	status, err = urr.Repo.EditarUsuario(t, token.IDUsuario)
	if err != nil {
		http.Error(w, MsgErrorEdit+err.Error(), 400)
		print(ErrorErr)
		return
	}

	if status == false {
		http.Error(w, "No se ha logrado modificar el registro del usuario ", 400)
		print(StatusError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(UpdateRegistryOk))
}

// Vehiculos//

/*ConsultarVehiculos route para vehiculos  por ID*/
func (urr *Route) ConsultarVehiculos(w http.ResponseWriter, r *http.Request) {
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	ID := r.URL.Query().Get("id")

	if len(ID) < 1 {
		http.Error(w, "Debe enviar el identificador del vehiculo", http.StatusBadRequest)
		return
	}
	vehiculo, err := urr.Repo2.ConsultarVehiculo(ID)
	if err != nil {
		http.Error(w, MsgErrorConsult+err.Error(), 400)
		return
	}

	w.Header().Set(CntxType, Type)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(vehiculo)
}

/*EditarVehiculo editar datos del vehiculo*/
func (urr *Route) EditarVehiculo(w http.ResponseWriter, r *http.Request) {
	var t models.Vehiculo
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, IncorrectData+err.Error(), 400)
		print(IncorrectData)
		return
	}

	var status bool

	status, err = urr.Repo2.EditarVehiculo(t, t.ID)
	if err != nil {
		http.Error(w, MsgErrorEdit+err.Error(), 400)
		print("error", err)
		return
	}

	if status == false {
		http.Error(w, "No se ha logrado modificar el registro del vehiculo ", 400)
		print(StatusError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(UpdateRegistryOk))
}

/*RegistroVehiculo es la función para crear en la BD el registro de vehiculos */
func (urr *Route) RegistroVehiculo(w http.ResponseWriter, r *http.Request) {
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	var t models.Vehiculo

	err := json.NewDecoder(r.Body).Decode(&t)
	print(t.Placa)
	if err != nil {
		http.Error(w, MsgError+err.Error(), 400)
		return
	}
	if len(t.Modelo) == 0 {
		http.Error(w, "El Modelo del vehiculo es requerido", 400)
		return
	}
	if len(t.Color) == 0 {
		http.Error(w, "Debe colocar un c", 400)
		return
	}

	_, encontrado, _ := usuario.CheckYaExisteVehiculo(t.Placa)
	if encontrado == true {
		http.Error(w, "Ya existe un vehiculo registrado con esa placa", 400)
		return
	}

	_, status, err := urr.Repo2.InsertoRegistroVehiculo(t)
	if err != nil {
		http.Error(w, "Ocurrio un error al intentar realizar el registro de usuario"+err.Error(), 400)
		return
	} else {
		print(MsgEntry)
	}

	if status == false {
		http.Error(w, "No se ha logrado insertar el registro del usuario", 400)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Se creo el vehiculo"))
}

/*GetAllvehiculo route para consultar todos los vehiculos*/
func (urr *Route) GetAllvehiculo(w http.ResponseWriter, r *http.Request) {
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	user, err := urr.Repo2.GetAllvehiculo()
	if err != nil {
		http.Error(w, MsgErrorAllConsult+err.Error(), 400)
		return
	}

	w.Header().Set(CntxType, Type)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(user)
}

//reservation//

/*ConsultarReserva route para vehiculos  por ID*/
func (urr *Route) ConsultarReserva(w http.ResponseWriter, r *http.Request) {
	ID := r.URL.Query().Get("id")
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	if len(ID) < 1 {
		http.Error(w, "Debe enviar el identificador del vehiculo", http.StatusBadRequest)
		return
	}
	vehiculo, err := urr.Repo3.ConsultarReserva(ID)
	if err != nil {
		http.Error(w, MsgErrorConsult+err.Error(), 400)
		return
	}

	w.Header().Set(CntxType, Type)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(vehiculo)
}

/*EditarReserva editar datos del vehiculo*/
func (urr *Route) EditarReserva(w http.ResponseWriter, r *http.Request) {
	var t models.Reserva
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, IncorrectData+err.Error(), 400)
		print(IncorrectData)
		return
	}

	var status bool
	var idReserva = t.ID.Hex()
	status, err = urr.Repo3.EditarReserva(t, idReserva)
	if err != nil {
		http.Error(w, MsgErrorEdit+err.Error(), 400)
		print("error", err)
		return
	}

	if status == false {
		http.Error(w, "No se ha logrado modificar el registro del vehiculo ", 400)
		print(StatusError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(UpdateRegistryOk))
}

/*InsertoRegistroReserva es la función para crear en la BD el registro de vehiculos */
func (urr *Route) InsertoRegistroReserva(w http.ResponseWriter, r *http.Request) {
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	var t models.Reserva

	err := json.NewDecoder(r.Body).Decode(&t)
	print(t.Fecha)
	if err != nil {
		http.Error(w, MsgError+err.Error(), 400)
		return
	}
	if len(t.Hora) == 0 {
		http.Error(w, "El Modelo del vehiculo es requerido", 400)
		return
	}
	if len(t.IDVehiculo) == 0 {
		http.Error(w, "Debe colocar un c", 400)
		return
	}

	_, encontrado, _ := usuario.CheckYaExisteReserva(t.ID)
	if encontrado == true {
		http.Error(w, "Ya existe una reserva registrado", 400)
		return
	}

	_, status, err := urr.Repo3.InsertoRegistroReserva(t)
	if err != nil {
		http.Error(w, "Ocurrio un error al intentar realizar el registro de reserva"+err.Error(), 400)
		return
	} else {
		print(MsgEntry)
	}

	if status == false {
		http.Error(w, "No se ha logrado insertar el registro del reserva", 400)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Se creo el reserva"))
}

/*GetAllReserva route para consultar todos los vehiculos*/
func (urr *Route) GetAllReserva(w http.ResponseWriter, r *http.Request) {
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	user, err := urr.Repo3.GetAllReserva()
	if err != nil {
		http.Error(w, MsgErrorAllConsult+err.Error(), 400)
		return
	}

	w.Header().Set(CntxType, Type)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(user)
}

/*CambiarEstadoReserva editar datos del usuario*/
func (urr *Route) CambiarEstadoReserva(w http.ResponseWriter, r *http.Request) {
	var t models.Reserva
	w.Header().Add(CntxType, Type)
	w.Header().Set(Cors, AllOrigin)
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		http.Error(w, IncorrectData+err.Error(), 400)
		print(IncorrectData)
		return
	}

	var status bool

	status, err = urr.Repo3.CambiarEstadoReserva(t, token.IDUsuario)
	if err != nil {
		http.Error(w, MsgErrorEdit+err.Error(), 400)
		print(ErrorErr)
		return
	}

	if status == false {
		http.Error(w, "No se ha logrado modificar el registro del reserva ", 400)
		print(StatusError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Se cambio el estado correctamente"))
}
