package middlew

import (
	"encoding/json"
	"net/http"
)

// MensajeError  respuesta erronea
type MensajeError struct {
	Status  bool   `json:"status"`
	Code    string `json:"code"`
	Message string `json:"message"`
}

// Map is a convenient way to create objects of unknown types.
type Response struct {
	Status  bool        `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

const Type = "application/json; charset=utf-8"
const CntxType = "content-type"
const Cors = "Access-Control-Allow-Origin"
const Cams = "Access-Control-Allow-Methods"
const Methods = "GET"
const AllOrigin = "*"
const CacheControl = "Cache-Control"
const CacheValue = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0"
const Vary = "Vary"
const Encoding= "Accept-Encoding"
type Map map[string]interface{}

// JSON standardized JSON response.
func JSON(w http.ResponseWriter, r *http.Request, statusCode int, data interface{}) error {
	setDefaultHeaders(w)
	if data == nil {
		w.Header().Set(CntxType, Type)
		w.WriteHeader(statusCode)
		return nil
	}

	j, err := json.Marshal(data)
	if err != nil {
		return err
	}

	w.Header().Set(CntxType, Type)
	w.WriteHeader(statusCode)
	_, _ = w.Write(j)
	return nil
}

// HTTPError standardized error response in JSON format.
func HTTPError(w http.ResponseWriter, r *http.Request, statusCode int, codeError string, message string) error {
	msg := MensajeError{
		Status:  false,
		Code:    codeError,
		Message: message,
	}

	return JSON(w, r, statusCode, msg)
}

func setDefaultHeaders(w http.ResponseWriter) {
	w.Header().Set(Cors, AllOrigin)
	w.Header().Set(Cams, Methods)
	w.Header().Set(CacheControl, CacheValue)
	w.Header().Set(CntxType, Type)
	w.Header().Set(Vary, Encoding)
}
