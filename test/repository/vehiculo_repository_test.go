package repository

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository/vehiculo"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

var (
	db2                = bd.New()
	vehiculoRepository = vehiculo.NewVehiculoRepo(db2)
)

func dataVehiculo() []models.Vehiculo {
	return []models.Vehiculo{
		{
			ID:          primitive.NewObjectID(),
			Placa:       "Kuy06B",
			Modelo:      "2008",
			Color:       "negro",
			Tipo:        "moto",
			Marca:       "AKT",
			Propietario: "cristhian",
		},
		{

		},
		{
			ID:          primitive.NewObjectID(),
			Placa:       "Kuy06B",
			Modelo:      "2008",
			Color:       "blanco",
			Tipo:        "moto",
			Marca:       "AKT",
			Propietario: "cristhian",
		},
	}
}

func TestInsertRegistryVehicle(t *testing.T) {
	_, status, _ := vehiculoRepository.InsertoRegistroVehiculo(dataVehiculo()[0])
	assert.Equal(t, status, true)
}

func TestInsertRegistryVehicleFail(t *testing.T) {
	_, status, _ := vehiculoRepository.InsertoRegistroVehiculo(dataVehiculo()[1])
	assert.Equal(t, status, true)
}

func TestUpdateVehicle(t *testing.T) {
	id, _ := primitive.ObjectIDFromHex("2")
	status, _ := vehiculoRepository.EditarVehiculo(dataVehiculo()[2], id)
	assert.Equal(t, status, true)
}

func TestUpdateVehicleFail(t *testing.T) {
	id, _ := primitive.ObjectIDFromHex("-2")
	status, _ := vehiculoRepository.EditarVehiculo(dataVehiculo()[2], id)
	assert.Equal(t, status, true)
}

func TestConsultVehicleById(t *testing.T) {
	vehicle, _ := vehiculoRepository.ConsultarVehiculo("2")
	assert.NotNil(t, vehicle)
}

func TestConsultVehicleByIdFail(t *testing.T) {
	vehicle, _ := vehiculoRepository.ConsultarVehiculo("-2")
	assert.NotNil(t, vehicle)
}
func TestGetAllVehicle(t *testing.T) {
	vehicle, _ := vehiculoRepository.GetAllvehiculo()
	assert.NotNil(t, vehicle)
}
