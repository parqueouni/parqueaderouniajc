package repository

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository/usuario"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

var (
	db                = bd.New()
	usuarioRepository = usuario.NewUserRepo(db)
)

// dataUSer is dataDB for test
func dataUSer() []models.Usuario {
	return []models.Usuario{
		{
			ID:       primitive.NewObjectID(),
			Nombre:   "cristhian",
			Apellido: "patarroyo",
			Email:    "cristian@gmail.com",
			Password: "112233",
			Estado:   "A",
		},
		{
			ID:       primitive.NewObjectID(),
			Nombre:   "felipe",
			Apellido: "rincon",
			Email:    "frinconn@gmail.com",
			Password: "123456",
			Estado:   "A",
		},
		{

		},
		{
			ID:       primitive.NewObjectID(),
			Nombre:   "Juan",
			Apellido: "patarroyo",
			Email:    "cristian@gmail.com",
			Password: "112233",
			Estado:   "A",
		},
	}
}

func TestLogin(t *testing.T) {

	_, estado := usuarioRepository.Login(dataUSer()[0].Email, dataUSer()[0].Password)
	assert.Equal(t, estado, true)
}

func TestLoginFailed(t *testing.T) {
	_, status := usuarioRepository.Login(dataUSer()[1].Email, dataUSer()[1].Password)
	assert.Equal(t, status, false)
}

func TestCambiarEstadousuario(t *testing.T) {
	res, _ := usuarioRepository.CambiarEstadousuario(dataUSer()[0], "I")
	assert.Equal(t, res, true)
}

func TestCambiarEstadousuarioFail(t *testing.T) {
	res, _ := usuarioRepository.CambiarEstadousuario(dataUSer()[2], "I")
	assert.NotEqual(t, res, true)
}

func TestInsertoRegistro(t *testing.T) {
	_, status, _ := usuarioRepository.InsertoRegistro(dataUSer()[0])
	assert.Equal(t, status, true)
}

func TestInsertRegistroFail(t *testing.T) {
	_, status, _ := usuarioRepository.InsertoRegistro(dataUSer()[2])
	assert.Equal(t, status, true)
}

func TestUpdate(t *testing.T) {
	status, _ := usuarioRepository.EditarUsuario(dataUSer()[3], "2")
	assert.Equal(t, status, true)
}

func TestUpdateFail(t *testing.T) {
	status, _ := usuarioRepository.EditarUsuario(dataUSer()[3], "-2")
	assert.Equal(t, status, true)
}

func TestConsultUser(t *testing.T) {
	usuario, _ := usuarioRepository.ConsultarUsuarioDB("2")
	assert.NotNil(t, usuario)
}

func TestConsultUserFail(t *testing.T) {
	usuario, _ := usuarioRepository.ConsultarUsuarioDB("-2")
	assert.NotNil(t, usuario)
}

func TestGetAllUser(t *testing.T) {
	users, err := usuarioRepository.GetAllUser()
	assert.NotNil(t, users)
	assert.Nil(t, err)
}
