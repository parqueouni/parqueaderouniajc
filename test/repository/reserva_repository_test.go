package repository

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	Reserva "bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository/reserva"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

var (
	dbs               = bd.New()
	reservaRepository = Reserva.NewReservaRepo(dbs)
)

// dataReserva is dataDB for test
func dataReserva() []models.Reserva {
	//id, _ := primitive.ObjectIDFromHex("2")

	return []models.Reserva{
		{
			ID:         primitive.NewObjectID(),
			IDUsuario:  "5fb72eddebef6eb28bb9065a",
			IDVehiculo: "5fbb208849fb6ad357ff8b13",
			Fecha:      "2020-12-12",
			Hora:       "08:00",
			Estado:     "A",
		},
		{

		},
		{
			ID:         primitive.NewObjectID(),
			IDUsuario:  "5fb72eddebef6eb28bb9065a",
			IDVehiculo: "5fbb208849fb6ad357ff8b13",
			Fecha:      "2020-12-12",
			Hora:       "08:00",
			Estado:     "A",
		},
		{
			ID:         primitive.NewObjectID(),
			IDUsuario:  "5fb72eddebef6eb28bb9065a",
			IDVehiculo: "5fbb208849fb6ad357ff8b13",
			Fecha:      "2020-12-12",
			Hora:       "08:00",
			Estado:     "I",
		},
	}
}

func TestInsertToRegistryReservation(t *testing.T) {
	_, status, _ := reservaRepository.InsertoRegistroReserva(dataReserva()[0])
	assert.Equal(t, status, true)
}

func TestInsertToRegistryReservationFail(t *testing.T) {
	_, status, _ := reservaRepository.InsertoRegistroReserva(dataReserva()[1])
	assert.Equal(t, status, true)
}

func TestUpdateReservation(t *testing.T) {
	status, _ := reservaRepository.EditarReserva(dataReserva()[2], "2")
	assert.Equal(t, status, true)
}

func TestUpdateReservationFail(t *testing.T) {
	status, _ := reservaRepository.EditarReserva(dataReserva()[2], "-2")
	assert.Equal(t, status, true)
}
func TestGetAllReservations(t *testing.T) {
	reservations, _ := reservaRepository.GetAllReserva()
	assert.NotNil(t, reservations)
}

func TestUpdateStatusActiveReservation(t *testing.T) {
	status, _ := reservaRepository.CambiarEstadoReserva(dataReserva()[2], "I")
	assert.Equal(t, status, true)
}

func TestUpdateStatusInactiveReservation(t *testing.T) {
	status, _ := reservaRepository.CambiarEstadoReserva(dataReserva()[3], "I")
	assert.Equal(t, status, true)
}

func TestUpdateStatusReservationFail(t *testing.T) {
	status, _ := reservaRepository.CambiarEstadoReserva(dataReserva()[1], "I")
	assert.NotEqual(t, status, true)
}

func TestConsultReservationById(t *testing.T) {
	reservation, _ := reservaRepository.ConsultarReserva("2")
	assert.NotNil(t, reservation)
}

func TestConsultReservationByIdFail(t *testing.T) {
	reservation, obj := reservaRepository.ConsultarReserva("5fcc1d140cd38b64e21a2b94")
	assert.NotNil(t, obj)
	assert.NotNil(t, reservation)
}
