package jwt

import (
	"time"

	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	jwt "github.com/dgrijalva/jwt-go"
)

/*GenerateJWT genera el token de acceso*/
func GenerateJWT(t models.Usuario) (string, error) {

	miClave := []byte("ParqueaderoProjectUniajc")

	payload := jwt.MapClaims{
		"email":     t.Email,
		"nombre":    t.Nombre,
		"apellidos": t.Apellido,
		"_id":       t.ID.Hex(),
		"exp":       time.Now().Add(time.Hour * 24).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)
	tokenStr, err := token.SignedString(miClave)
	if err != nil {
		return tokenStr, err
	}
	return tokenStr, nil
}
