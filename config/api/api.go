package api

import "bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"

func Start(port string) {

	// connection to the database.
	db := bd.New()
	server := newServer(port, db)
	server.StartServer(db)
}
