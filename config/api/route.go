package api

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/handlers"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/middlew"
	"github.com/gorilla/mux"
	"net/http"
)

func Routes(conn *bd.Data) http.Handler {
	ur := handlers.NewUserHandler(conn)

	return routes(ur)
}

/*routes retorna los puntos de acceso*/
func routes(handler *handlers.Route) http.Handler {
	router := mux.NewRouter()

	//servicios usuarios
	router.HandleFunc("/registro", middlew.ChequeoBD(handler.Registro)).Methods("POST")
	router.HandleFunc("/login", middlew.ChequeoBD(handler.Login)).Methods("POST")
	router.HandleFunc("/consultarUsuario", middlew.ValidateJWT(handler.ConsultarUsuario)).Methods("GET")
	router.HandleFunc("/consultarTodos", middlew.ValidateJWT(handler.GetAllUser)).Methods("GET")
	router.HandleFunc("/editarUsuario", middlew.ValidateJWT(handler.EditarUsuario)).Methods("POST")
	router.HandleFunc("/cambiarEstadoUsuario", middlew.ValidateJWT(handler.CambiarEstadoUsuario)).Methods("POST")
	//servicios vehiculos
	router.HandleFunc("/registroVehiculo", middlew.ValidateJWT(handler.RegistroVehiculo)).Methods("POST")
	router.HandleFunc("/editarVehiculo", middlew.ValidateJWT(handler.EditarVehiculo)).Methods("POST")
	router.HandleFunc("/consultarVehiculo", middlew.ValidateJWT(handler.ConsultarVehiculos)).Methods("GET")
	router.HandleFunc("/consultarVehiculos", middlew.ValidateJWT(handler.GetAllvehiculo)).Methods("GET")
	//servicios reserva
	router.HandleFunc("/registroReserva", middlew.ValidateJWT(handler.InsertoRegistroReserva)).Methods("POST")
	router.HandleFunc("/editarReserva", middlew.ValidateJWT(handler.EditarReserva)).Methods("POST")
	router.HandleFunc("/consultarReserva", middlew.ValidateJWT(handler.ConsultarReserva)).Methods("GET")
	router.HandleFunc("/consultarReservas", middlew.ValidateJWT(handler.GetAllReserva)).Methods("GET")
	router.HandleFunc("/inactivarReserva", middlew.ValidateJWT(handler.CambiarEstadoReserva)).Methods("POST")
	return router
}
