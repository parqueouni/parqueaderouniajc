package api

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"github.com/rs/cors"
	"log"
	"net/http"
	"time"
)

type Server struct {
	*http.Server
}

// ServeHTTP implements the http.Handler interface for the server type.
func (srv *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	srv.Handler.ServeHTTP(w, r)
}
func (srv *Server) StartServer(conn *bd.Data) {
	router := Routes(conn)
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:8080", "http://ec2-18-218-124-150.us-east-2.compute.amazonaws.com:8181"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"},
		AllowedHeaders:   []string{"Authorization", "Content-Type", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials"},
		AllowCredentials: true,
		Debug:            true,
	})

	handler := c.Handler(router)
	log.Fatal(http.ListenAndServe(":"+"8181", handler))
}

func newServer(port string, conn *bd.Data) *Server {

	router := Routes(conn)

	s := &http.Server{
		Addr:         ":" + port,
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	return &Server{s}
}
