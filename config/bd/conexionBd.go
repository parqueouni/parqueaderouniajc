package bd

import (
	"context"
	"log"
	"sync"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	data *Data
	once sync.Once
)

type Data struct {
	DB *mongo.Client
}

/*MongoCN es el objeto de conexion a la base de datos*/
var MongoCN = ConnectarBD()
var clientOptions = options.Client().ApplyURI("mongodb+srv://parqueouniajcestudianteS941:parqueouniajcestudianteS94135498261@cluster0.5dsqg.mongodb.net/parqueouniajc?retryWrites=true&w=majority")

func New() *Data {
	ConnectarBD()
	return data
}

/*ConnectarBD es la funcion de conectar a la conexion la la base de datos*/
func ConnectarBD() *mongo.Client {
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err.Error())
		return client
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err.Error())
		return client
	}
	log.Println("Conexión Exitosa con la BD")
	data = &Data{
		DB: client,
	}
	return client
}

/*CheckConnection es el ping a la base de datos*/
func CheckConnection() int {
	err := MongoCN.Ping(context.TODO(), nil)
	if err != nil {
		return 0
	}
	return 1
}
