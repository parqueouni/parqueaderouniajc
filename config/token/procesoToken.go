package token

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/repository/usuario"
	"errors"
	"strings"

	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/models"
	"github.com/dgrijalva/jwt-go"
)

/*Email valor de Email usado en todos los EndPoints*/
var Email string

/*IDUsuario es el ID devuelto del modelo */
var IDUsuario string

/*ProcesoToken proceso token para extender sus valores*/
func ProcesoToken(tk string) (*models.Claim, bool, string, error) {
	miClave := []byte("ParqueaderoProjectUniajc")
	claims := &models.Claim{}

	splitToken := strings.Split(tk, "Bearer")
	if len(splitToken) != 2 {
		return claims, false, "", errors.New("formato de token invalido")
	}

	tk = strings.TrimSpace(splitToken[1])

	tkn, err := jwt.ParseWithClaims(tk, claims, func(token *jwt.Token) (interface{}, error) {
		return miClave, nil
	})
	if err == nil {
		_, encontrado, _ := usuario.CheckYaExisteUsuario(claims.Email)
		if encontrado == true {
			Email = claims.Email
			IDUsuario = claims.ID.Hex()
		}
		return claims, encontrado, IDUsuario, nil
	}
	if !tkn.Valid {
		return claims, false, "", errors.New("token Inválido")
	}
	return claims, false, "", err
}
